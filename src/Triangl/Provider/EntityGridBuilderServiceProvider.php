<?php

namespace Triangl\Provider;

use Silex\ServiceProviderInterface;

use Triangl\Component\EntityGridBuilder;

/**
 * Service used to build grids based on entities.
 */
class EntityGridBuilderServiceProvider implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function register(\Silex\Application $app)
    {
        $app['db.orm.grid'] = $app->share(function ($app) {
            return new EntityGridBuilder($app);
        });
    }

    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app)
    {
    }
}
