<?php

namespace Triangl\Provider;

use Silex\ServiceProviderInterface;

use Triangl\Component\EntityFormBuilder;

/**
 * Service used to build forms based on entities.
 */
class EntityFormBuilderServiceProvider implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function register(\Silex\Application $app)
    {
        $app['db.orm.form'] = $app->share(function ($app) {
            return new EntityFormBuilder($app);
        });
    }

    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app)
    {
    }
}
