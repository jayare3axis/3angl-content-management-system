<?php

namespace Triangl\Provider;

use Silex\ServiceProviderInterface;

use Triangl\Component\Assets\PageTitle;

/**
 * Service that handles value of Html page title tag.
 */
class PageTitleServiceProvider implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function register(\Silex\Application $app)
    {
        $app['page_title'] = $app->share(function ($app) {
            return new PageTitle($app);
        });
    }

    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app)
    {
    }
}
