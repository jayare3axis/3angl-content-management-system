<?php

namespace Triangl\Provider;

use Silex\ServiceProviderInterface;

use Triangl\Component\Assets\Assets;

/**
 * Service that handles value of Html page title tag.
 */
class AssetsServiceProvider implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function register(\Silex\Application $app)
    {
        $app['assets'] = $app->share(function ($app) {
            return new Assets($app["twig"]);
        });
    }

    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app)
    {
    }
}
