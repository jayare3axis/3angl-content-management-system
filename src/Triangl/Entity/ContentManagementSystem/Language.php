<?php

namespace Triangl\Entity\ContentManagementSystem;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\NameTrait;

/**
 * Language entity.
 * @Entity @Table(name="languages")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class Language {
    use PrimaryIdTrait;    
    use NameTrait;
}
