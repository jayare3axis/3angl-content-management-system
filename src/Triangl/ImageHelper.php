<?php

namespace Triangl;

/**
 * Helper routines for images.
 */
class ImageHelper {
    private $app;
    
    /**
     * Default constructor.
     */
    public function __construct(Application $app) {
        $this->app = $app;
    }
    
    /**
     * Returns true if media under given path is image.
     * @param string $path
     * @return boolean
     */
    public function isImage($path) {
        $a = getimagesize($path);
        $imageType = $a[2];

        if (in_array( $imageType , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP) ) )
        {
            return true;
        }
        return false;
    }
}
