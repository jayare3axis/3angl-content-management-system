<?php

namespace Triangl;

use Silex\ServiceProviderInterface;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\FormServiceProvider;

use Triangl\Provider\PageTitleServiceProvider;
use Triangl\Provider\AssetsServiceProvider;
use Triangl\Provider\TwigMenuRendererServiceProvider;
use Triangl\Provider\TwigGridRendererServiceProvider;
use Triangl\Provider\EntityFormBuilderServiceProvider;
use Triangl\Provider\EntityGridBuilderServiceProvider;

/*
 * Triangl content management system module.
 */
class ContentManagementSystem implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app) {
        $app["asset.path.spinner"] = $app["asset.path"] . "/images/spinner.gif";
    }

    /**
     * Implemented.
     */
    public function register(\Silex\Application $app) {
        // Default configurations.
        $app["name"] = "3angl Website";
        $app["asset.path"] = "";        
        $app["jquery.path"] = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js";
        $app["twig.values"] = array(
            "twig.options" => array(
                "cache" => $app->isDebug() ? false : $app["path"] . "/twig"
            )
        );
        $app["translation.values"] = array(
            'locale_fallbacks' => array('en')
        );
        $app["form.values"] = array(); 
        
        // Create dir for Twig cache if necessary.
        if ( isset($app["twig.values"]["twig.options"]["cache"]) ) {
            $path = $app["twig.values"]["twig.options"]["cache"];
            if ( is_string($path) ) {
                $engine = $app["triangl.engine"];  
                $engine->createDirIfNotExist($path);
            }
        }
        
        // Register entities.
        $app['triangl.entities']->addNamespace( array(
            'type' => 'annotation',
            'path' => __DIR__ . '/Entity/ContentManagementSystem',
            'namespace' => 'Triangl\Entity\ContentManagementSystem'
        ) );
        
        // Add domain filter.
        $app['triangl.entities']->addFilter('domain', '\Triangl\Entity\DomainFilter');
        
        // Register services.
        $app['triangl.image'] = $app->share( function ($app) {
            return new ImageHelper($app);
        } );
        $app->register( new PageTitleServiceProvider() );
        $app->register( new AssetsServiceProvider() );
        $app->register( new UrlGeneratorServiceProvider() );
        $app->register( new TwigServiceProvider(), $app["twig.values"] );
        $app->register( new TranslationServiceProvider(), $app["translation.values"] );
        $app->register( new ValidatorServiceProvider() );
        $app->register( new FormServiceProvider(), $app["form.values"] );        
        $app->register( new EntityFormBuilderServiceProvider() );
        $app->register( new EntityGridBuilderServiceProvider() );
        
        // Register template folder.
        $app['twig.loader.filesystem']->prependPath(__DIR__ . "/../../views");
        
        // Override Twig loader to be able to render strings.
        $app['twig.loader.string'] = $app->share(function ($app) {
            return new \Twig_Loader_String();
        });
        $app['twig.loader'] = $app->share(function ($app) {
            return new \Twig_Loader_Chain(array(
                $app['twig.loader.array'],
                $app['twig.loader.filesystem'],
                $app['twig.loader.string']
            ));
        });
    }
}
