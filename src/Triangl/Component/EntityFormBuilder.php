<?php

namespace Triangl\Component;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;

use Silex\Application;

/**
 * Default form builder implementation.
 */
class EntityFormBuilder implements EntityFormBuilderInterface {
    private $app;
    
    /**
     * Default constructor.
     */
    public function __construct(Application $app) {
        $this->app = $app;
    }
    
    /**
     * Implemented.
     */
    public function createForm($className, $id = null, $properties = null) {
        $data = array();
        
        // Get values.
        $properties = $this->app['triangl.entities']->getProperties($className, $properties);
        foreach ($properties as $name => $property) {
            if ($id == null) {
                $data[$name] = $this->getDefaultValue($className, $property);
            }
            else {
                $data[$name] = $this->getPropertyValue($className, $id, $property);
            }
        }
        
        // Build form.
        $builder = $this->app->form($data);
        $first = true;
        foreach ($properties as $name => $property) {
            $options = array();
            if ($first) {
                $options['attr'] = array('autofocus' => '');
            }

            // TO - DO handle validation
            //$options['constraints'] = array( new Assert\NotBlank() );

            $this->createInputForProperty($builder, $className, $id, $name, $options);
            $first = false;
        }
        
        return $builder->getForm();
    }
    
    /**
     * Adds input for given property.
     */
    protected function createInputForProperty(\Symfony\Component\Form\FormBuilder $builder, $className, $id, $property, array $options = array()) {        
        $fieldType = $this->onDetermineFieldType($className, $property);
        
        if ($fieldType == null) {
            return;
        }
        
        // TO - DO determine order field
        if ($property == 'ord') {
            $fieldType = 'hidden';
        }
        
        if ($property == 'ord' && $id != null) {
            // Do not add ord field for edited record.
        }
        else if ($fieldType == 'file' && $id == null) {            
            // Do not add file fields for new records.
        }        
        else {
            if ($fieldType == 'file') {
                $options['attr'] = array(
                    "data-url" => $this->app->path( "backend_upload", array(
                        "className" => $className,
                        "id" => $id,
                        "property" => $property
                    ) )
                );
                $options['required'] = false;
            }
            $builder->add($property, $fieldType, $options);
        }
    }
    
    /**
     * Determines field type for given property.
     * @param string $className
     * @param string $property
     */
    protected function onDetermineFieldType($className, $property) {
        $metaData = $this->app['db.orm.em']->getClassMetaData($className);
        
        // Field is reference to another object.
        if ( $metaData->hasAssociation($property) ) {
            if ( $metaData->isCollectionValuedAssociation($property) ) {
                return null;
            }
            return 'hidden';
        }
        
        // Field is numeric.
        if ( $metaData->getTypeOfField($property) == 'smallint' ) {
            return 'integer';
        }
        if ( $metaData->getTypeOfField($property) == 'integer' ) {
            return 'integer';
        }
        if ( $metaData->getTypeOfField($property) == 'bigint' ) {
            return 'integer';
        }
        
        // Field is file.
        if ( $metaData->getTypeOfField($property) == 'object' ) {
            return 'file';
        }
        
        return 'text';
    }
    
    /**
     * Gets default value of property.
     * @param string $className
     * @param string $property
     * @return mixed
     */
    protected function getDefaultValue($className, $property) {
        // TO - DO implement
        
        // This is hard-coded implementation to be obsolete.
        if ( $property->getName() == 'domain' ) {
            return $this->app['backend.selector.domain']->getSelectedDomain()->getId();
        }
        
        return null;
    }
    
    /**
     * Gets value of property of target instance.
     * @param string $className
     * @param int $id
     * @param string $property
     * @return mixed
     */
    protected function getPropertyValue($className, $id, $property) {
        $metaData = $this->app['db.orm.em']->getClassMetaData($className);
        $instance = $this->app['db.orm.em']->getRepository($className)->find($id);
        
        $val = $property->getValue($instance);        
                
        if ( is_object($val) && ( get_class($val) == 'Doctrine\ORM\PersistentCollection' || is_subclass_of($val, 'Doctrine\ORM\PersistentCollection') ) ) {
            $val = null;
        }
        
        if ( is_object($val) && $val != null ) {            
            if ( $metaData->hasAssociation( $property->getName() ) ) {
                // If value is entity set value as it's primary key
                $metaData = $this->app['db.orm.em']->getClassMetaData( get_class($val) );
                $val = $metaData->getSingleIdReflectionProperty()->getValue($val);
            }
            else {
                // TO - DO this is for file, determine it somehow
                $path = $this->app['backend.upload.fm']->getUploadDir() . "/" . $val->getName();
                $val = new File($path);
            }            
        }
        
        return $val;
    }
}
