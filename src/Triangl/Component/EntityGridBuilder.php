<?php

namespace Triangl\Component;

use Silex\Application;

use Triangl\Component\Grid\Grid;
use Triangl\Component\Grid\GridRow;
use Triangl\Component\Grid\GridCell;
use Triangl\Component\Grid\GridPager;

/**
 * Default grid builder implementation.
 */
class EntityGridBuilder implements EntityGridBuilderInterface {
    private $app;
    
    const MAX_ROWS = 10;
    
    /**
     * Default constructor.
     */
    public function __construct(Application $app) {
        $this->app = $app;
    }
    
    /**
     * Implemented.
     */
    public function createGrid($className, $page = 0, $properties = null, $rows = null, $criteria = array(), $order = null) {        
        $grid = new Grid();
        
        // Create header
        $header = array( new GridCell('#') );
        $properties = $this->app['triangl.entities']->getProperties($className, $properties);
        foreach ($properties as $name => $property) {
            array_push( $header, new GridCell($name) );
        }
        $row = new GridRow($header);
        $row->setHeader();
        $grid->pushRow($row);
        
        // Fill rows
        $repository = $this->app['db.orm.em']->getRepository($className);
        $num = 0;
        $cnt = 0;
        if ($rows === null) {
            $rows = array();
            if ($page != -1) {
                $pagedResult = $repository->findPage($page, self::MAX_ROWS);
                $rows = $pagedResult->getResult();
                $num = $page * self::MAX_ROWS;
                $cnt = $pagedResult->getCount();
            }
            else {
                $rows = $repository->findBy($criteria, $order); 
                $cnt = count($rows);
            }
        }
        
        // Pass count to client.
        $grid->addData("count", $cnt);
        
        $first = true;
        foreach ($rows as $row) {
            $row = $this->createRowFromInstance(++$num, $row, $properties);
            if ($first) {
                $row->setActive();
            }
            $grid->pushRow($row);
            $first = false;
        }
        
        // Handle pager.
        if ($page != -1) {
            $pages = 1;
            if ( $pagedResult->getCount() > 0 ) {
                $pages = ceil($pagedResult->getCount() / self::MAX_ROWS);
            }
            $grid->setPager( new GridPager($pages, $page) );
        }
        
        $grid->addData("entity", $className);
        
        return $grid;
    }
    
    /**
     * Gets row based on intance.
     * @param object $instance
     * @param array $properties
     * @return Triangl\Control\GridRow
     */
    protected function createRowFromInstance($num, $instance, array $properties) {
        $cells = array();
        
        array_push( $cells, new GridCell($num) );
        
        foreach ($properties as $property) {
            $data = $property->getValue($instance);
            // Handle collection.
            if ( is_object($data) && get_class($data) == 'Doctrine\ORM\PersistentCollection' ) {
                $data = 'Total: ' . $data->count();
            }
            if ( is_object($data) && get_class($data) == 'DateTime' ) {
                // TO - DO handle localized date time formatting
                $data = $data->format('d. m. Y');
            }
            if ( is_object($data) ) {
                try {
                    $data = $data->__toString();
                } 
                catch (\Exception $e) {
                    $data = '';
                }
            }
            array_push( $cells, new GridCell($data) );
        }
        
        $id = $this->app['triangl.entities']->getId($instance);
        
        $row = new GridRow($cells);
        $row->addData('id', $id);
        // TO - DO this should be handled by grid widget
        $row->addData( 'form-url', $this->app->url( 'widget_form', array(
            'className' => '\\' . get_class($instance),
            'id' => $id
        ) ) );
                
        return $row;
    }
}
