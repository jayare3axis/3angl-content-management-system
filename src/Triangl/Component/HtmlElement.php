<?php

namespace Triangl\Component;

/**
 * A html element.
 */
class HtmlElement {
    private $data;
    private $classes;
    
    /**
     * Default constructor.
     */
    public function __construct() {
        $this->data = array();
        $this->classes = array();
    }
    
    /**
     * Adds data under specified key.
     * @param type $key
     * @param type $val
     */
    public function addData($key, $val) {
        $this->data[$key] = $val;
    }
    
    /**
     * Gets all data or data under specified key.
     * @return mixed
     */
    public function getData($key = null) {
        if ($key == null) {
            return $this->data;
        }
        if ( !array_key_exists($key, $this->data) ) {
            throw new \InvalidArgumentException("Data with key: $key not found within Html element.");
        }
        return $this->data[$key];
    }
    
    /**
     * Adds a class.
     * @param string $class
     */
    public function addClass($class) {
        $this->classes[$class] = true;
    }
    
    /**
     * Gets the data.
     * @return array
     */
    public function getClasses() {
        return ( count($this->classes) == 0 ) ? array() : array_keys($this->classes);
    }
}
