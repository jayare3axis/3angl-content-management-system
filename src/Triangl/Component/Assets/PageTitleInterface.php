<?php

namespace Triangl\Component\Assets;

/**
 * Handles setting and getting of html head title.
 */
interface PageTitleInterface {
    /**
     * Gets title.
     */
    public function getTitle();
    
    /**
     * Sets title.
     * @param string $title value
     */
    public function setTitle($title);
}
