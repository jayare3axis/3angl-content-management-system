<?php

namespace Triangl\Component\Assets;

/**
 * Asset item.
 */
abstract class AssetItem {
    protected $values;
    protected $twig;
    
    /**
     * Default constructor.
     */
    public function __construct($twig, array $values) {
        $this->twig = $twig;
        $this->values = $values;        
    }
    
    /**
     * Implement to render asset item.
     */
    public abstract function render();
}
