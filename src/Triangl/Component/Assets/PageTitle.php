<?php

namespace Triangl\Component\Assets;

use Silex\Application;

/**
 * Page title interface imlementation.
 */
class PageTitle implements PageTitleInterface {
    private $app;
    private $title;
    
    /**
     * Default constructor.
     */
    public function __construct(Application $app) {
        $this->app = $app;
        $this->title = $app["name"];
    }
    
    /**
     * Implemented.
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Implemented.
     */
    public function setTitle($title) {
        $this->title = $title . " | " . $this->app["name"];
    }

}
