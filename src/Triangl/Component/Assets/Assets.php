<?php

namespace Triangl\Component\Assets;

/**
 * Manages HTML access, css, javascript and image files used for icons etc.
 */
class Assets implements AssetsInterface {
    private $twig;    
    private $js = array();
    private $css = array();
    
    /**
     * Default constructor.
     * @param Twig_Environment $twig
     */
    public function __construct(\Twig_Environment $twig) {
        $this->twig = $twig;
    }
    
    /*
     * Implemented.
     */
    public function addStyleSheet(array $values) {
        array_push( $this->css, new StyleSheet($this->twig, $values) );
    }
    
    /*
     * Implemented.
     */
    public function addJavaScript(array $values) {
        array_push( $this->js, new JavaScript($this->twig, $values) );
    }
    
    /**
     * Implemented
     */
    public function render() {
        return $this->twig->render('assets.html.twig', array(
            'js' => $this->js,
            'css'=> $this->css
        ) );
    }
}
