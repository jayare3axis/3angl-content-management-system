<?php

namespace Triangl\Component\Assets;

/**
 * A JavaScript item.
 */
class JavaScript extends AssetItem {
    /**
     * Implemented.
     */
    public function render() {
        $values = $this->values;
        return $this->twig->render('javascript.html.twig', array(
            'path' => ( isset($values["path"]) ) ? $values["path"] : null,
            'template' => ( isset($values["template"]) ) ? $values["template"] : null
        ) );
    }
}
