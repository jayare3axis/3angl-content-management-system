<?php

namespace Triangl\Component\Assets;

/**
 * Manages HTML access, css, javascript and image files used for icons etc.
 */
interface AssetsInterface {
    /**
     * Adds css stylesheet.
     * @param string $path url path
     */
    public function addStyleSheet(array $values);
    
    /**
     * Adds a javascript.
     * @param string $path url path
     */
    public function addJavaScript(array $values);
    
    /**
     * Render content;
     */
    public function render();
}
