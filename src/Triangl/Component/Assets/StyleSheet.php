<?php

namespace Triangl\Component\Assets;

/**
 * CSS style sheet.
 */
class StyleSheet extends AssetItem {
    /**
     * Implemented.
     */
    public function render() {
        $values = $this->values;
        return $this->twig->render('stylesheet.html.twig', array(
            'path' => ( isset($values["path"]) ) ? $values["path"] : null,
            'template' => ( isset($values["template"]) ) ? $values["template"] : null  
        ) );
    }
}
