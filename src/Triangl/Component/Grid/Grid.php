<?php

namespace Triangl\Component\Grid;

use Triangl\Component\HtmlElement;

/**
 * A grid in a UI.
 */
class Grid extends HtmlElement {
    private $rows;
    private $pager;
    
    /**
     * Default constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->rows = array();
        $this->pager = null;
    }
            
    /**
     * Gets active row or null.
     * @return Triangl\Navigation\Row
     */
    public function getActiveRow() {
        foreach ($this->rows as $row) {
            if ( $row->isActive() ) {
                return $row;
            }
        }
        return null;
    }
    
    /**
     * Gets the row count.
     * @return int row count.
     */
    public function getRowCount() {
        return count($this->rows);
    }
    
    /**
     * Gets the row collection.
     * @return array
     */
    public function getRows() {
        return $this->rows;
    }
    
    /**
     * Gets row at given position.
     * @param int $pos
     * @return Triangl\Control\Grid
     */
    public function getRowAt($pos) {
        return ( $pos >= 0 && $pos < $this->getRowCount() ) ? $this->rows[$pos] : null;
    }
    
    /**
     * Adds row to the grid.
     * @param GridRow $row
     * @return Triangl\Control\Grid this
     */
    public function pushRow(GridRow $row) {
        array_push($this->rows, $row);
        return $this;
    }
    
    /**
     * Adds grid pager to the grid.
     * @param Triangl\Control\GridPager $pager
     * @return Triangl\Control\Grid
     */
    public function setPager(GridPager $pager) {
        $this->pager = $pager;        
        return $this;
    }
    
    /**
     * Gets grid pager.
     */
    public function getPager() {
        return $this->pager;
    }
}
