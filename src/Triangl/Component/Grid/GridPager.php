<?php

namespace Triangl\Component\Grid;

use Triangl\Component\Navigation\MenuItemComposite;

/**
 * Displays grid paging informations and navigation controls.
 */
class GridPager {
    private $count;
    private $index;
    private $menu;
    
    /**
     * Default constructor.
     * @param int $count count of pages
     * @param int $index active page index
     */
    public function __construct($count, $index) {
        $this->count = $count;
        $this->index = $index;
        $this->menu = null;
    }
    
    /**
     * Gets count of pages in pager.
     */
    public function getCount() {
        return $this->count;
    }
    
    /**
     * Gets active page index.
     * @return int active page
     */
    public function getIndex() {
        return $this->index;
    }
    
    /**
     * Sets menu used with pager.
     * @param \Triangl\Component\Navigation\MenuItemComposite $menu
     */
    public function setMenu(MenuItemComposite $menu) {
        $this->menu = $menu;
    }
    
    /**
     * Gets menu used with pager.
     * @return \Triangl\Component\Navigation\MenuItemComposite
     */
    public function getMenu() {
        return $this->menu;
    }
}
