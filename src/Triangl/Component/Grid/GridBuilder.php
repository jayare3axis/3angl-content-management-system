<?php

namespace Triangl\Component\Grid;

/**
 * Build grids for UI part.
 */
class GridBuilder {
    private $grids;
    
    /**
     * Default constructor.
     */
    public function __construct() {
        $this->grids = array();
    }
    
    /**
     * Gets the grids in a builder.
     */
    public function getGrids() {
        return $this->grids;
    }
    
    /**
     * Gets count of grids in a builder.
     * @return int count
     */
    public function getCount() {
        return count($this->grids);
    }
    
    /**
     * Gets grid at given position.
     * @param int $pos
     * @return Triangl\Component\Grid\Grid
     */
    public function getGridAt($pos) {
        return ( $pos >= 0 && $pos < $this->getCount() ) ? $this->grids[$pos] : null;
    }
    
    /**
     * Gets the grid with active set to true or null if none found.
     * @return Triangl\Component\Grid\Grid
     */
    public function getActiveGrid() {
        foreach ($this->grids as $grid) {
            if ( $grid->getActiveRow() !== null ) {
                return $grid;
            }
        }
        return null;
    }
    
    /**
     * Adds new grid to the builder.
     * @param Triangl\Component\Grid\Grid $grid
     * @return Triangl\Component\Grid\GridBuilder this
     */
    public function pushGrid(Grid $grid) {
        array_push($this->grids, $grid);
        return $this;
    }
}
