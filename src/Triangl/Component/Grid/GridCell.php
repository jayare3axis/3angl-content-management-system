<?php

namespace Triangl\Component\Grid;

/**
 * A cell in a grid row.
 */
class GridCell {
    private $data;
    
    /**
     * Default constructor.
     */
    public function __construct($data = null) {
        $this->data = $data;
    }
    
    /**
     * Sets cell data.
     * @param mixed $data
     * @return Triangl\Control\GridCell this
     */
    public function setData($data) {
        $this->data = $data;
        return $this;
    }
    
    /**
     * Gets cell data.
     * @return mixed cell data
     */
    public function getData() {
        return $this->data;
    }
}
