<?php

namespace Triangl\Component\Grid;

/**
 * Row in a grid.
 */
class GridRow {
    private $cells;
    private $active;
    private $header;
    private $data;
    
    /**
     * Default constructor.
     */
    public function __construct( array $cells = array() ) {
        $this->cells = $cells;
        $this->active = false;
        $this->header = false;
        $this->data = array();
    }
    
    /**
     * Makes the grid row active.
     * @param bool $val specified value
     * @return Triangl\Control\GridRow this
     */
    public function setActive($val = true) {
        $this->active = $val;
        return $this;
    }
    
    /**
     * Returns true if row is active.
     * @return boolean
     */
    public function isActive() {
        return $this->active;
    }
    
    /**
     * Makes the grid row header.
     * @param bool $val specified value
     * @return Triangl\Control\GridRow this
     */
    public function setHeader($val = true) {
        $this->header = $val;
        return $this;
    }
    
    /**
     * Returns true if row is header.
     * @return boolean
     */
    public function isHeader() {
        return $this->header;
    }
    
    /**
     * Adds data under specified key.
     * @param type $key
     * @param type $val
     */
    public function addData($key, $val) {
        $this->data[$key] = $val;
    }
    
    /**
     * Gets the data.
     * @return array
     */
    public function getData() {
        return $this->data;
    }
    
    /**
     * Gets the cell count.
     * @return int cell count.
     */
    public function getCellCount() {
        return count($this->cells);
    }
    
    /**
     * Gets the cell collection.
     * @return array
     */
    public function getCells() {
        return $this->cells;
    }
    
    /**
     * Gets cell at given position.
     * @param int $pos
     * @return Triangl\Control\GridCell
     */
    public function getCellAt($pos) {
        return ( $pos >= 0 && $pos < $this->getCellCount() ) ? $this->cells[$pos] : null;
    }
}
