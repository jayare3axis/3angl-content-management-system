<?php

namespace Triangl\Component;

/**
 * Builds forms for entities.
 */
interface EntityFormBuilderInterface {
    /**
     * Returns form instance based on given entity.
     * @param string $className fully quallified class name
     * @param int $id entity id
     * @param array $properties array of properties or null for default
     * @return Symfony\Component\Form\Form
     */
    public function createForm($className, $id = null, $properties = null);
}
