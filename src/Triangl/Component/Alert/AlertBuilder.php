<?php

namespace Triangl\Component\Alert;

/**
 * Build alert messages for UI part.
 */
class AlertBuilder {
    private $alerts;
    
    /**
     * Default constructor.
     */
    public function __construct() {
        $this->alerts = array();
    }
    
    /**
     * Gets the grids in a builder.
     */
    public function getAlerts() {
        return $this->alerts;
    }
    
    /**
     * Gets count of alerts in a builder.
     * @return int count
     */
    public function getCount() {
        return count($this->alerts);
    }
    
    /**
     * Gets grid at given position.
     * @param int $pos
     * @return Triangl\Control\Alert\Alert
     */
    public function getAlertAt($pos) {
        return ( $pos >= 0 && $pos < $this->getCount() ) ? $this->alerts[$pos] : null;
    }
    
    /**
     * Adds new grid to the builder.
     * @param Triangl\Component\Alert\Alert $alert
     * @return Triangl\Component\Alert\AlertBuilder this
     */
    public function pushAlert(Alert $alert) {
        array_push($this->alerts, $alert);
        return $this;
    }
}
