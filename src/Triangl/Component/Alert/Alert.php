<?php

namespace Triangl\Component\Alert;

/**
 * An alert message in a UI.
 */
class Alert {
    private $message;
    private $type;
    private $icon;
    
    /**
     * Default constructor.
     */
    public function __construct($message, $type, $icon = null) {
        $this->message = $message;
        $this->type = $type;
        $this->icon = $icon;
    }
    
    /**
     * Gets alert message.
     * @return string message
     */
    public function getMessage() {
        return $this->message;
    }
    
    /**
     * Gets alert type.
     * @return string type
     */
    public function getType() {
        return $this->type;
    }
    
    /**
     * Gets alert icon.
     * @return string icon
     */
    public function getIcon() {
        return $this->icon;
    }
}
