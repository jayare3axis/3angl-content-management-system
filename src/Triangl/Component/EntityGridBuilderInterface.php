<?php

namespace Triangl\Component;

/**
 * Builds grids for entities.
 */
interface EntityGridBuilderInterface {
    /**
     * Returns grid instance based on given entity.
     * @param string $className fully quallified class name
     * @param int $page which page to display
     * @param array $properties array of properties or null for default
     * @return Triangl\Control\Grid
     */
    public function createGrid($className, $page = 0, $properties = null);
}
