<?php

namespace Triangl\Component\Navigation;

use Triangl\Component\HtmlElement;

/**
 * Item in a menu base class.
 */
abstract class MenuItem extends HtmlElement {
    private $caption;
    private $icon;
    private $target;
    private $active;
    private $disabled;
    
    /**
     * Default constructor
     */
    public function __construct($caption = "", $route = null, $icon = null, $target = "self") {
        $this->caption = $caption;
        $this->route = $route;
        $this->icon = $icon;
        $this->target = $target;
        $this->active = false;
        $this->disabled = false;
        $this->args = array();
    }
    
    /**
     * Makes menu item active.
     * @param bool $val value
     * @return Triangl\Navigation\MenuItem this
     */
    public function setActive($val = true) {
        $this->active = $val;
        return $this;
    }
    
    /**
     * Makes menu item disabled.
     * @return Triangl\Navigation\MenuItem this
     */
    public function setDisabled($val = true) {
        $this->disabled = $val;
        return $this;
    }
    
    /**
     * Returns true if menu is active.
     */
    public function isActive() {
        return $this->active;
    }
    
    /**
     * Returns true if menu is disabled.
     */
    public function isDisabled() {
        return $this->disabled;
    }
    
    /**
     * Gets the caption.
     */
    public function getCaption() {
        return $this->caption;
    }
    
    /**
     * Gets the route.
     */
    public function getRoute() {
        return $this->route;
    }
    
    /**
     * Gets the icon.
     */
    public function getIcon() {
        return $this->icon;
    }
    
    /**
     * Gets the target.
     */
    public function getTarget() {
        return $this->target;
    }
    
    /**
     * Adds route argument.
     * @param string $name
     * @param string $val
     * @return Triangl\Navigation\MenuItem this
     */
    public function pushArg($name, $val) {
        $this->args[$name] = $val;
        return $this;
    }
    
    /**
     * Gets route arguments.
     */
    public function getArgs() {
        return $this->args;
    }
}
