<?php

namespace Triangl\Component\Navigation;

/**
 * A menu item composite.
 */
class MenuItemComposite extends MenuItem {
    protected $children;
    
    /**
     * Default constructor
     */
    public function __construct($caption = "", $href = null, $icon = null, $target = "self") {
        parent::__construct($caption, $href, $icon, $target);        
        $this->children = array();
    }
    
    /**
     * Gets count of items in a menu.
     * @return int count
     */
    public function getCount() {
        return count($this->children);
    }
    
    /**
     * Gets children.
     * @param int $pos gets child or null
     */
    public function getChild($pos) {
        return ( $pos >= 0 && $pos < count($this->children) ) ? $this->children[$pos] : null;
    }
    
    /**
     * Searches menu for caption.
     * @param string $route
     * @param array $args
     * @return result or null
     */
    public function findChild($route, array $args = array()) {
        foreach ($this->children as $child) {
            if ( strcmp( $child->getRoute(), $route ) == 0 ) {
                $match = true;
                $a = $child->getArgs();
                foreach ($args as $key => $val) {
                    if ( !array_key_exists($key, $a) ) {
                        $match = false;
                    }
                    else {
                        $arg = $a[$key];
                        if ($arg != $val) {
                            $match = false;
                        }
                    }
                }
                if ($match) {
                    return $child;
                }
            }  
            if ($child instanceof MenuItemComposite && $child->findChild($route, $args) != null) {
                return $child->findChild($route, $args);
            }
        }
        return null;
    }
    
    /**
     * Gets active menu item.
     * @return Triangl\Navigation\MenuItem $item menu item.
     */
    public function getActiveChild() {
        foreach ($this->children as $child) {
            if ( $child->isActive() ) {
                return $child;
            }
            if ($child instanceof MenuItemComposite && $child->getActiveChild() != null) {
                return $child->getActiveChild();
            }
        }
        return null;
    }
    
    /**
     * Gets children collection.
     * @return array children collection.
     */
    public function getChildren() {
        return $this->children;
    }
    
    /**
     * Pushes child to the item menu.
     * @param Triangl\Navigation\MenuItem $item item to push
     * @return Triangl\Navigation\MenuItemComposite this
     */
    public function pushChild(MenuItem $item) {
        array_push($this->children, $item);
        return $this;
    }
}
