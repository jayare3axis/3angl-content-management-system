<?php

namespace Triangl\Component\Navigation;

/**
 * Builds menu for the UI part.
 */
class MenuBuilder extends MenuItemComposite {    
    /**
     * Overriden.
     */
    public function pushChild(MenuItem $item, $autoselect = false) {
        // Select first item
        // TO - DO make better implementation, make autoselect obsolete
        if ($autoselect == true) {
            if ( count($this->children) == 0 ) {
                $menu = $item->getChild(0);
                if ($menu) {
                    $menu->setActive();
                }
            }
        }
        
        // Do not add empty composite menus.
        if ($item instanceof MenuItemComposite) {
            if ( $item->getCount() == 0 ) {
                return $this;
            }
        }
        
        array_push($this->children, $item);
        return $this;
    }
    
    /**
     * Selects menu item by given route.
     * @param string $route
     * @param array $args
     */
    public function selectByRoute($route, array $args = array()) {
        $this->reset();
        $menu = $this->findChild($route, $args);
        if ($menu != null) {
            $menu->setActive();
        }
    }
    
    /**
     * Makes non menu item active.
     * @return Triangl\Navigation\MenuBuilder this
     */
    protected function reset() {
        foreach ($this->children as $menu) {
            foreach ($menu->getChildren() as $child) {
                $child->setActive(false);
            }
        }
    }
}
