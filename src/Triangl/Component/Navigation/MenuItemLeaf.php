<?php

namespace Triangl\Component\Navigation;

/**
 * A menu item leaf.
 */
class MenuItemLeaf extends MenuItem {
    /**
     * Default constructor
     */
    public function __construct($caption = "", $href = null, $icon = null, $target = "self") {
        parent::__construct($caption, $href, $icon, $target);
    }
}
