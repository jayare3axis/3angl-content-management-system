<?php

namespace Triangl;

use Silex\Application\UrlGeneratorTrait;
use Silex\Application\TwigTrait;
use Silex\Application\TranslationTrait;
use Silex\Application\FormTrait;

/**
 * Application with content management system module.
 */
class ContentManagementSystemApplication extends SecurityApplication {
    use UrlGeneratorTrait;
    use TwigTrait;
    use TranslationTrait;
    use FormTrait;
    
    /**
     * Overriden.
     */
    protected function init() {
        parent::init();
        
        $this->register( new ContentManagementSystem() );
    }
}
