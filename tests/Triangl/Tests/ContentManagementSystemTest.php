<?php

namespace Triangl\Tests;

use Triangl\WebTestCase;
use Triangl\ContentManagementSystemApplication;

/**
 * Functional test for Triangl Content Management System.
 */
class ContentManagementSystemTest extends WebTestCase {
    /**
     * Implemented.
     */
    public function createApplication() {
        return new ContentManagementSystemApplication( 
            __DIR__ . "/../../../var", 
            array("test" => true) 
        );
    }
}
